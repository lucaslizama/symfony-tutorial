<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Job;
use App\Service\JobHistoryService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;

class CategoryController extends AbstractController
{
    /**
     * Finds and displays a category entity
     * 
     * @Route(
     *      "/category/{slug}", 
     *      name="category.show", 
     *      methods="GET"
     * )
     * 
     * @param Category $category
     * @param PaginatorInterface $paginator
     * @param Request $request
     * 
     * @return Response
     */
    public function show(Request $request, Category $category, PaginatorInterface $paginator, JobHistoryService $jobHistoryService): Response
    {
        $page = $request->query->get("page");
        $activeJobs = $paginator->paginate(
            $this->getDoctrine()->getRepository(Job::class)->getPaginatedActiveJobsByCategoryQuery($category),
            $page == null ? 1 : $page, //page
            $this->getParameter("max_jobs_on_category") //elements per page
        );

        return $this->render("category/show.html.twig", [
            "category" => $category,
            "activeJobs" => $activeJobs,
            "historyJobs" => $jobHistoryService->getJobs(),
        ]);
    }
}
